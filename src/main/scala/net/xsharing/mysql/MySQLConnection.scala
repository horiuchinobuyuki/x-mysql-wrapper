/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.xsharing.mysql
import java.sql.{DriverManager, Connection, Statement, ResultSet,SQLException}


/**
 * @constructor MySQLへのコネクション
 * @param host
 * @param port
 * @param database
 * @param user
 * @param password
 */
class MySQLConnection (
  val host:String = "localhost",
  val port:Int = 3306,
  val database:String = "", 
  val user:String = "",
  val password:String = "",
  val otherParams:String = ""
) {

  private lazy val con = {
    Class.forName("com.mysql.jdbc.Driver").newInstance();
    DriverManager.getConnection("jdbc:mysql://"+this.host+":"+this.port.toString+"/"+this.database+"?"+this.otherParams ,
					this.user, this.password)
  }		    			

  def executeSelect(sql:String, print:Boolean = false):List[Map[String, Object]] = {
    if(print) {    
      println("executing query: \n"+ sql);
    }
    
    val st = this.con.createStatement()
    
    val rs = st.executeQuery(sql)
    val md = rs.getMetaData
    
    val list = scala.collection.mutable.MutableList[Map[String, Object]]()
    
    while(rs.next) {
      val r = (1 to md.getColumnCount).map{n => md.getColumnLabel(n)->rs.getObject(n)}.toMap
      list += r;
    }
    rs.close
    st.close
    return list.toList;
  }
  
  
  
  def executeUpdate(sql:String, print:Boolean = false):Int = {
    if(print) {    
      println("executing query: \n"+ sql);
    }
    
    
    val st = this.con.createStatement();
    val num:Integer = st.executeUpdate(sql);
    st.close()
    
    return num
  }
  
  def executeInsert(sql:String, print:Boolean = false):Int = {
    executeUpdate(sql, print)
  }
  
  def executeDelete(sql:String, print:Boolean = false):Int = {
    executeUpdate(sql, print)
  }
  
  def executeBatch(sqls:List[String], print:Boolean = false):List[Int] = {
    if(print) {    
      println("executing queries: \n"+ sqls.mkString("\n"))
    }
    
    val st = this.con.createStatement();
    for (sql <- sqls) {st.addBatch(sql)}
    val ret = st.executeBatch
    st.close();
    
    return ret.toList
  }
  
  def metaData():java.sql.DatabaseMetaData = {
    this.con.getMetaData()
  }
  
  def close():Unit = {
    this.con.close()
  }
}


